import React from 'react';
import "./Registerpiece.scss"
import {Link} from "react-router-dom";
import Button from "../Buttons/Button"
import {login} from "../../routes";

const Registerpiece = ({userType, userTypeContent}) => {
    return(
        <div className="Registerpiece">
            <h1>
                How to Register
            </h1>
            <p className="userType">As <span>{userType}</span></p>
            <p className="userContent">{userTypeContent}</p>
            <Link to={login}><Button btnData="Register Now" btnStyle="#E61F01"/></Link>
        </div>
    )
}

export default Registerpiece;