import React from 'react';
import "./Footer.scss"
import Fb from "../../assets/icons/facebook.svg";
import twt from "../../assets/icons/twitter.svg";
import ig from "../../assets/icons/instagram.svg";
import ellipsis from "../../assets/icons/ellipsis.png";

function Footer(){
    return(
        <>
            <div className="Footer flex">
                <div className="Footer-social flex">
                    <a href="#">
                        <img src={twt} alt="twitter"/>
                    </a>
                    <a href="">
                        <img src={Fb} alt="facebook"/>
                    </a>
                    <a href="">
                        <img src={ig} alt="instagram"/>
                    </a>
                    
                </div>
                <div className="Footer-menu flex">
                    <p>&copy; 2020 All Rights Reserved</p>
                    <div className="Footer-menu__links flex">
                        <a href="#">terms and condition</a>
                        <img src={ellipsis} alt="ellipsis"/>
                        <a href="#">privacy policy</a>
                        <img src={ellipsis} alt="ellipsis"/>
                        <a href="#">cookie policy</a>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Footer;