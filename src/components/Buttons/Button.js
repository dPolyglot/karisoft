import React from 'react';
import "./Button.scss"

function Button({btnData, btnStyle}){
    return(
        <>
          <button className="Button" style={{backgroundColor: btnStyle}}>{btnData}</button>
        </>
    )
}

export default Button;