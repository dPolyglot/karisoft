import React from "react";
import "./Navbar.scss";
import logo from "../../assets/images/logo.png";
import menu from "../../assets/icons/align-justify.svg";
import {Link} from "react-router-dom";
import {kariInvest, register} from "../../routes";

const Navigation = (props) => {
    return(
        <div className="Navbar">
            <Link to={kariInvest}>
                <div className="Navicon Navigation-logo">
                    <img src={logo} alt="karisoft logo"/>
                </div>
            </Link>

            <Link to={register}>
                <div className="Navigation-menu">
                    <img src={menu} alt="menu"/>
                </div>
            </Link>
        </div>
    )
}

export default Navigation;