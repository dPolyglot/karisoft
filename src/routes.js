export const home = '/'
export const pageNotFound = '/pagenotfound'
export const kariInvest = '/invest'
export const register = '/register'
export const login = '/login'
export const trader = '/trader'
export const investor = '/investor'
export const student = '/student'
export const registerForm = '/register-form'