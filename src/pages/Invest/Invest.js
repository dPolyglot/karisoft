import React from "react";
import "./Invest.scss"
import Navbar from "../../components/Navigation/Navbar";
import Button from "../../components/Buttons/Button";
import {Link} from "react-router-dom";
import {trader, investor} from "../../routes";
import investImage from "../../assets/images/e.png";

function Invest(){
    return(
        
        <div className="Invest bg-blue">
            <Navbar/>
            <div className="Container">
                <div className="Container-content Invest-content">
                    <div className="Invest-content__left">
                        <h2>Helping You Invest  Towards a Better Future!</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sed erat vitae dolor pharetra finibus. Suspendisse .</p>
                        <div className="Invest-btn flex">  
                            <Link to={investor}><Button btnData="Become an Investor"/></Link>  
                            <Link to={trader}><Button btnData="Register as a Trader"/></Link>   
                        </div>
                    </div>
                    <div className="Big-image">
                        <img src={investImage} alt=""/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Invest