import React from "react";
import Navbar from "../../components/Navigation/Navbar";
import "./NotFound.scss";

function NotFound(){
    return(
        <div className="Notfound bg-blue">
            <Navbar/>
            <div className="Container">
                <div className="Container-content">
                    <h1>404 <br/> Page not found</h1>
                </div>
            </div>
        </div>
    )
}

export default NotFound;