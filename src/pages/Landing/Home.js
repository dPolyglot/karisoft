import React from 'react'
import "./Home.scss";
import logo from "../../assets/images/logo.png";
import Button from "../../components/Buttons/Button";
import { Link } from "react-router-dom"
import {pageNotFound, kariInvest} from "../../routes"

const Home = (props) => {
    return (
        <div className="Home">
            <div className="Container Home-body">
                <div className="Home-logo">
                    <img src={logo} alt="karisoft logo" />
                </div>
                <div className="Home-authBtn">
                    <Link to={pageNotFound}><Button btnData="Karisoft Solutions" /></Link>
                    <Link to={kariInvest}><Button btnData="KariSoft Finances" /></Link>
                </div>
            </div>
        </div>
    )
}

export default Home