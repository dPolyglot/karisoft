import React, {useState} from 'react';
import "./Auth.scss";
import Navbar from "../../components/Navigation/Navbar"
import registerImage from "../../assets/images/icon.png";
import dropDown from "../../assets/icons/dropdown.svg";
import {Link} from "react-router-dom";
import {investor, trader, student} from "../../routes";

const Register = props => {
    
    const [isOpen, setToggle] = useState(false);

    const showRegisterOptions = () => {setToggle(!isOpen)}

    return(
        <div className="Auth-register bg-blue">
           
            <Navbar/>
            <div className="Container">
                <div className="Container-content Auth-register--content flex">
                    <div className="Auth-register--content__left flex" onClick={showRegisterOptions}>
                        <h2>How to register</h2>
                        <div className="Auth-register--content__left-img">
                            <img src={dropDown} alt="arrow facing downward"/>
                        </div>
                        {isOpen ? 
                            <div className="Auth-dropdown">
                                <Link to={investor}>
                                    <div className="Auth-invest__link">Register as an investor</div>
                                </Link>
                                <Link to={trader}>
                                    <div className="Auth-invest__link">Register as a trader</div>
                                </Link>
                                <Link to={student}>
                                    <div className="Auth-invest__link">Register as a student</div>
                                </Link>
                            </div> 
                        : null}
                    </div>
                    <div className="Big-image-2">
                        <img src={registerImage} alt=""/>
                    </div>
                    
                </div>
            </div>
        </div>
    )
}

export default Register