import React from 'react';
import "./Auth.scss"
import Navbar from "../../components/Navigation/Navbar"

const Login = props => {
    return(
        <div className="Auth-login bg-blue">
            <Navbar/>
            <div className="Container">
                <div className="Container-content Auth-login-body">

                </div>
            </div>
        </div>
    )
}

export default Login;