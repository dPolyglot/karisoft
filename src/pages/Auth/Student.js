import React from 'react';
import "./Auth.scss";
import Navbar from "../../components/Navigation/Navbar";
import Registerpiece from "../../components/Registerpiece/Registerpiece";
import {registerForm} from "../../routes"

const Student = props => {
    return(
        <div className="Auth-student">
            <Navbar/>
            <div className="Container">
                <div className="Container-content flex Auth__content">
                    <Registerpiece
                        userType="a Student"
                        userTypeContent="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sed erat vitae dolor pharetra finibus. Suspendisse ut pharetra turpis. Praesent justo sapien, dictum in enim at, efficitur imperdiet diam. Aliquam purus magna, iaculis eget tincidunt nec, interdum quis ante. Integer eu ultrices mauris, ac elementum nisi"
                    />
                </div>
            </div>
        </div>
    )
}

export default Student