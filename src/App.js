import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.scss';
import Footer from "./components/Footer/Footer";
import NotFound from "./pages/404/NotFound";
import KariFinance from "./pages/Invest/Invest";
import Home from './pages/Landing/Home';
import KariLogin from './pages/Auth/Login';
import KariRegister from './pages/Auth/Register';
import KariInvestor from './pages/Auth/Investor';
import KariTrader from './pages/Auth/Trader';
import KariStudent from './pages/Auth/Student';
import KariRegisterForm from './pages/Auth/RegisterForm';
import * as routes from './routes'


function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path={routes.home} exact component={Home} />
          <Route path={routes.pageNotFound} component={NotFound} />
          <Route path={routes.kariInvest} component={KariFinance} />
          <Route path={routes.login} component={KariLogin}/>
          <Route path={routes.register} component={KariRegister}/>
          <Route path={routes.trader} component={KariTrader}/>
          <Route path={routes.student} component={KariStudent}/>
          <Route path={routes.investor} component={KariInvestor}/>
          <Route path={routes.registerForm} component={KariRegisterForm}/>
        </Switch>
        <div className="App-footer">
          <Footer />
        </div>
      </Router>
    </div>
  );
}

export default App;
